<?php

require_once("./Clothes/ClothForSleep.php");
require_once("./Clothes/ClothForMorning.php");
require_once("./Clothes/ClothForDinner.php");
require_once("./Clothes/ClotheForEvening.php");


/**
 * Чаще всего, абстрактная фабрика создает объекты через статичные методы
 * Плюс - это простота и доступность из любой части кода
 * Минус - сложность замены логики фабрики на другую (но для этого больше подходит шаблон "Фабричный Метод")
 */
class SimpleFabric {

    /**
     * создаем объект с "Одеждой" на основе текущего времени
     * @var current_hour - часы прогулки
     * 
     * Обычно сюда передают строку с названием нужного класса, 
     * однако фабрика может быть более интелектуальной и можно передать что то более специализированное
     */
    public static function getInstance($current_hour):ICloth {
        echo "текущее время: ", $current_hour, " ч.";

        //для часов сна
        if($current_hour >= 22 && $current_hour <= 24){           
            return new ClothForSleep();
        }
        if($current_hour >= 0 && $current_hour < 6){           
            return new ClothForSleep();
        }

        //для утренней прогулки
        if($current_hour >= 6 && $current_hour < 12){
            return new ClothForMorning();
        }

        //для послеобеденного времени
        if($current_hour >= 12 && $current_hour < 18){
            return new ClothForDinner();
        }

        //на вечер
        if($current_hour >= 18 && $current_hour < 22){
            return new ClotheForEvening();
        }

    }

}
