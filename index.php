<?php
require_once("SimpleFabric.php");
require_once("WhatToWhear.php");

/**
 * Будем просить нашу фабрику, создать обект с Одеждой, но в разное время
 */

$time = 6; //утро
$cloth = SimpleFabric::getInstance($time);
$what_to_wear_now = new WhatToWhear($cloth);
$what_to_wear_now->getRecommendation();


$time = 14; //послеобеденное время
$cloth = SimpleFabric::getInstance($time);
$what_to_wear_now = new WhatToWhear($cloth);
$what_to_wear_now->getRecommendation();


$time = 17; //вечернее время
$cloth = SimpleFabric::getInstance($time);
$what_to_wear_now = new WhatToWhear($cloth);
$what_to_wear_now->getRecommendation();


$time = 22; //время сна
$cloth = SimpleFabric::getInstance($time);
$what_to_wear_now = new WhatToWhear($cloth);
$what_to_wear_now->getRecommendation();