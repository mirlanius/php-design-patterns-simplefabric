<?php
require_once("./Clothes/ICloth.php");

/**
 * Модель с логикой приложения.
 * Рекомендует, что сейчас одеть из одежды
 */
class WhatToWhear {

    private ICloth $cloth;
    
    public function __construct(ICloth $cloth){
        $this->cloth = $cloth;
    }

    public function getRecommendation(){
       echo "рекомендую одеться так:", PHP_EOL;

       echo "- ", $this->cloth->getHat(), PHP_EOL;
       echo "- ", $this->cloth->getMediumCloth(), PHP_EOL;
       echo "- ", $this->cloth->getFootWear(), PHP_EOL;

       echo PHP_EOL;
    }

}
