<?php

interface ICloth {

    public function getHat();
    public function getMediumCloth();
    public function getFootWear();

}
