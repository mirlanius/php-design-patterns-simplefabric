<?php
require_once("./Clothes/ICloth.php");

class ClothForDinner implements ICloth {

    public function getHat(){
        return "шляпа";
    }

    public function getMediumCloth(){
        return "брюки";
    }

    public function getFootWear(){
         return "туфли";
    }
}