<?php
require_once("./Clothes/ICloth.php");

class ClotheForEvening implements ICloth {

    public function getHat(){
        return "капюшон";
    }

    public function getMediumCloth(){
        return "куртка";
    }

    public function getFootWear(){
         return "ботинки";
    }
}