<?php
require_once("./Clothes/ICloth.php");

class ClothForMorning implements ICloth {

    public function getHat(){
        return "шапка для бега";
    }

    public function getMediumCloth(){
        return "водолазка";
    }

    public function getFootWear(){
         return "кроссовки";
    }
}